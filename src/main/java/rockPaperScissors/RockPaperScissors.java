package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true){
        
            System.out.println("Let's play round " + roundCounter);
            String humanMove = humanInput();
      
      
            // Getting input for computer 
            Random random = new Random();
            int randomNumber = random.nextInt(3);

            String computerMove;
            if (randomNumber == 0) {
                computerMove = rpsChoices.get(0);
            } else if (randomNumber == 1) {
                computerMove = rpsChoices.get(1);
            } else {
                computerMove = rpsChoices.get(2);
            }

            // Finding the winner of the round 

            String outcome;
            if (humanMove.equals(computerMove)) {
                outcome = "It's a tie!";
            }else if (playerWins(humanMove, computerMove)){
                humanScore++;
                outcome = "Player Wins!";
            }else {
                computerScore++;
                outcome = "Computer Wins!";
            }
        
            // Printing the output and score

            System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". " + outcome); 
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // Continue or break 

            String answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (answer.equals("y")){
                roundCounter++;
                continue;
            }else{
                break;
            }
        
    }

    System.out.println("Bye bye :)");

    }
        
    private String humanInput() {
        while (true) {
            String human = readInput("Your choice (Rock/Paper/Scissors)? ").toLowerCase();
            if (rpsChoices.contains(human)){
                return human;
            } else {
                System.out.println("I do not understand " + human + ". Could you try again?");
            }
        }
    }


    static boolean playerWins(String humanMove, String computerMove) {
        if (humanMove.equals("rock")) {
            return computerMove.equals("scissors");
        } else if (humanMove.equals("paper")) {
            return computerMove.equals("rock");
        } else {
            return computerMove.equals("paper");
        }
    
    }
   
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
